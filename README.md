# Pianoroll2midi

**Python script to convert pianoroll video to MIDI file.**


The final goal of this script is converting piano roll videos (like those generated with [Synthesia](https://www.synthesiagame.com/)) to regular music sheets.


### Getting started

1. Download video you want to convert ([youtube-dl](http://ytdl-org.github.io/youtube-dl/) is a nice tool)
2. Convert the video to a MIDI file with the script
3. Import the MIDI file in [MuseScore](https://musescore.org) to convert it into a music sheet



### Acknowledgement

Inspired from the blog post [Transcribing Piano Rolls, the Pythonic Way](http://zulko.github.io/blog/2014/02/12/transcribing-piano-rolls/)
