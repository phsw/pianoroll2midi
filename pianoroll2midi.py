from moviepy.editor import VideoFileClip
from mido import Message, MidiFile, MidiTrack, MetaMessage, bpm2tempo
from tqdm import tqdm
import sys
import math


# Display obtained image with the reading horizontal line:
PLOT_IMAGE = False

VERBOSE = False

# Correct timedeltas to correct pressed and released note times:
MAGNET_NOTE_DURATIONS = True

# Notes can be "magnetized" to this subtime (1 is a quarter note, so 0.5 is an eighth):
SMALLEST_NOTE_LENGTH = 0.5

# height of line reading piano roll:
HORIZONTAL_LINE = 400

# width, in pixel of bar representing a note:
NOTE_WIDTH = 6

# color threshold: if pixel color is superior, it means there is a note here:
NOTE_COLOR_THRESHOLD = 145

# threshold to convert x position to note:
NOTE_X_THRESHOLD = 4

FIRST_OCTAVE_ID = -1
LAST_OCTAVE_ID = 7

X_FIRST_C = 48
OCTAVE_WIDTH = 221 - X_FIRST_C
X_NOTES = {
    'c': 60 - X_FIRST_C,
    'c#': 70 - X_FIRST_C,
    'd': 84 - X_FIRST_C,
    'd#': 99 - X_FIRST_C,
    'e': 109 - X_FIRST_C,
    'f': 134 - X_FIRST_C,
    'f#': 142 - X_FIRST_C,
    'g': 158 - X_FIRST_C,
    'g#': 170 - X_FIRST_C,
    'a': 183 - X_FIRST_C,
    'a#': 198 - X_FIRST_C,
    'b': 208 - X_FIRST_C
}

NOTES_MIDI_CODES = {
    'c': 0,
    'c#': 1,
    'd': 2,
    'd#': 3,
    'e': 4,
    'f': 5,
    'f#': 6,
    'g': 7,
    'g#': 8,
    'a': 9,
    'a#': 10,
    'b': 11
}

MIDI_VELOCITY = 67

MIDI_EXTENSION = '.mid'


if PLOT_IMAGE:
    from matplotlib import pyplot as plt


def debug(*args, **kwargs):
    if VERBOSE:
        print(*args, *kwargs)


def is_unknwon_note(note):
    return note[0] == '?'


def convert_labeled_note_to_midi(label):
    note_label = label[:-1]
    height = int(label[-1]) + 2

    midi_note = (height * len(NOTES_MIDI_CODES)) + NOTES_MIDI_CODES[note_label]

    assert(midi_note > 0 and midi_note < 128)

    return midi_note


def convert_x_to_note(x):
    x_note = (x - X_FIRST_C) % OCTAVE_WIDTH
    letter = '?'
    height = int((x - X_FIRST_C) // OCTAVE_WIDTH)
    assert(height >= FIRST_OCTAVE_ID and height <= LAST_OCTAVE_ID)

    for note, x in X_NOTES.items():
        if abs(x - x_note) < NOTE_X_THRESHOLD:
            letter = note
            break

    return letter + str(height)


# Get x-centered positions of enabled notes in the selected frame, being aware which hand plays notes:
def analyze_line_for_hands(frame):
    notes = [[], []]
    in_note_left = False
    in_note_right = False
    note_begin = -1

    for i in range(len(frame)):
        if in_note_left:
            if frame[i][2] < NOTE_COLOR_THRESHOLD:
                in_note_left = False
                if i-note_begin > NOTE_WIDTH:
                    notes[0].append(convert_x_to_note((note_begin + i) / 2))

        if in_note_right:
            if frame[i][1] < NOTE_COLOR_THRESHOLD:
                in_note_right = False
                if i-note_begin > NOTE_WIDTH:
                    notes[1].append(convert_x_to_note((note_begin + i) / 2))

        if not in_note_left and not in_note_right:
            if frame[i][2] > NOTE_COLOR_THRESHOLD:
                in_note_left = True
                note_begin = i
            elif frame[i][1] > NOTE_COLOR_THRESHOLD:
                in_note_right = True
                note_begin = i

        assert(not (in_note_left and in_note_right))


    return notes


class PlayingHand:
    def __init__(self, midi_track: MidiTrack, ticks_per_frame: int, ticks_per_beat: int):
        self.started = False
        self.previous_notes = None
        self.nb_empty_frames = 0
        self.time_delta = 0
        self.midi_track = midi_track
        self.ticks_per_frame = ticks_per_frame
        self.ticks_per_beat = ticks_per_beat

    def _write_changes_to_track(self, new_notes, time_delta):
        change = False

        if MAGNET_NOTE_DURATIONS:
            min_length = int(self.ticks_per_beat * SMALLEST_NOTE_LENGTH)
            reminder = int(time_delta % min_length)

            if reminder > (min_length / 2):
                # increase time_delta:
                time_delta += (min_length - reminder)
            else:
                # decrease time_delta:
                time_delta -= reminder

            assert(time_delta >= 0)
            assert((time_delta % min_length) == 0)

        # look for released notes:
        for note in self.previous_notes:
            if (not is_unknwon_note(note)) and note not in new_notes:
                self.midi_track.append(Message('note_off', note=convert_labeled_note_to_midi(note), velocity=MIDI_VELOCITY, time=time_delta))
                change = True
                time_delta = 0

        # look for new pressed notes:
        for note in new_notes:
            if note not in self.previous_notes:
                if is_unknwon_note(note):
                    print("Warning: unrecognized note")
                else:
                    self.midi_track.append(Message('note_on', note=convert_labeled_note_to_midi(note), velocity=MIDI_VELOCITY, time=time_delta))
                    change = True
                    time_delta = 0

        return change

    def process_frame_notes(self, notes, other_hand_started: bool):
        # Wait for first note to start writing midi file (avoid silence at the beginning of the file):
        # (the first note can be played by the other hand)
        if (not self.started) and (other_hand_started or ((not self.started) and len(notes) > 0)):
            self.started = True

            if other_hand_started:
                # if we start because of the other hand, it means we missed the first frame:
                self.nb_empty_frames = 1

        if self.started:
            # Count empty frames:
            # * empty frame can be just the line between two contiguous notes: skip this frame. We accept no more than 2 frames in this case
            # * many empty frames can be a silent: adjust timedelta to take into account this silence
            if len(notes) > 0 and self.nb_empty_frames > 0:
                if self.nb_empty_frames <= 2:
                    self.time_delta = 0
                else:
                    self.time_delta = self.ticks_per_frame * self.nb_empty_frames

            if self.previous_notes is not None:  # can't compare with previous notes during the first iteration of the frame loop
                written_changes = self._write_changes_to_track(notes, self.time_delta)
                if written_changes:
                    self.time_delta = self.ticks_per_frame
                else:
                    self.time_delta += self.ticks_per_frame

            if len(notes) == 0:
                self.nb_empty_frames += 1
            else:
                self.nb_empty_frames = 0

        self.previous_notes = notes



if len(sys.argv) < 3 or len(sys.argv) > 5:
    print("Usage:")
    print("%s <video file> <BPM> [<start time (s)> [<end time (s)>]]" % sys.argv[0])
    sys.exit(1)

video_filename = sys.argv[1]
bpm = int(sys.argv[2])
start_time = 0
if len(sys.argv) > 3:
    start_time = int(sys.argv[3])
end_time = None
if len(sys.argv) > 4:
    end_time = int(sys.argv[4])


if PLOT_IMAGE and (start_time == 0 or end_time is None):
    print("You have to limit analyzed video length when plotting image is enabled, to avoid out of memory.")
    sys.exit(1)


midi_file = MidiFile()
track_left_hand = MidiTrack()
track_right_hand = MidiTrack()
midi_file.tracks.append(track_right_hand)
midi_file.tracks.append(track_left_hand)  # add left hand after right hand, so staves are in good order when importing in MuseScore
track_left_hand.append(MetaMessage('set_tempo', tempo=int(bpm2tempo(bpm))))
track_left_hand.append(MetaMessage('track_name', name='left_hand'))
track_right_hand.append(MetaMessage('track_name', name='right_hand'))

video = VideoFileClip(video_filename).subclip(start_time, end_time)

if video.w != 1280 or video.h != 720:
    print("Video has wrong size (%dx%d, instead of 1280*720)." % (video.width, video.height))
    sys.exit(1)

ticks_per_frame = round(bpm *  midi_file.ticks_per_beat / (60 * video.fps))

left_hand = PlayingHand(track_left_hand, ticks_per_frame, midi_file.ticks_per_beat)
right_hand = PlayingHand(track_right_hand, ticks_per_frame, midi_file.ticks_per_beat)

image = []
frame_id = 0
number_frames = int(math.ceil(video.duration * video.fps))

for frame in tqdm(video.iter_frames(), total=number_frames):
    debug("Frame #" + str(frame_id))

    if PLOT_IMAGE:
        image.append(frame[HORIZONTAL_LINE,])

    notes = analyze_line_for_hands(frame[HORIZONTAL_LINE,])
    debug(notes)

    left_hand.process_frame_notes(notes[0], right_hand.started)
    right_hand.process_frame_notes(notes[1], left_hand.started)

    frame_id += 1


midi_filename = video_filename[:video_filename.find('.')] + MIDI_EXTENSION
midi_file.save(midi_filename)

if PLOT_IMAGE:
    plt.imshow(image)
    plt.show()
